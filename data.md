# Usercentrics TagAudit für {% domain %}
version 1.5

> Die Seite {% domain %} wurde von Usercentrics analysiert. Ziel ist es, das Laden von externen Codes / Tags / Technolgien im Hinblick auf die neuen Regelungen der Datenschutzgrundverordnung DSGVO / GDPR, die ab 25.05.2018 in Kraft tritt, zu analysieren.

{%kpis ./assets/kpisSinglePage.json %}

## Executive Summary

Ab dem 25.05.2018 gilt weltweit die neue Datenschutzgrundverordnung (DSGVO) mit dem Ziel u.a. die persönlichen Daten von EU-Bürgern besser zu schützen. Strafen von 20 Mio. EUR oder bis zu 4% vom weltweiten Jahresumsatz sollen Unternehmer dazu motivieren, datenschutzkonform zu handeln (https://gdpr-info.eu/art-83-gdpr/). Die ePrivacy Verordnung wird voraussichtlich 2020 in Kraft treten und die DSGVO spezifischer auslegen, ihr jedoch nicht widersprechen.

1. Die DSGVO gilt für **jeden Identifier**, auch Cookie-Identifier, die zur Profilierung nutzbar sind. Diese werden nun als **personenbezogene Daten** angesehen; bisher - gemäß TMG - war dies auf die IP-Adresse und typische personenbezogene Daten wie z.B. die Adresse begrenzt).

2. Die Seite {% domain %} lädt insgesamt die oben genannte **Anzahl an Tags** z.B. externe Technologien. Teilweise enthält die Liste der Technologien auch Anbieter, deren Geschäftsmodell in der Weitergabe, der Nutzung oder dem Verkauf von personenbezogenen Daten liegt z.B. Audience Targeting, Retargeting, DMP.

3. Die Abfrage der Einwilligung (eng. "Consent") des Besuchers zur Weitergabe der Daten an Dritte / Firmen liegt **nicht** vor. Die Information und Einwilligung des Nutzers muss nach DSGVO **zum Zeitpunkt der Datenerhebung** erfolgen. Die Einwilligung ist nach DSGVO Voraussetzung für die Nutzung von Identifiern, insbesondere von Cookies fremder Technologieanbieter (https://gdpr-info.eu/recitals/no-30/ und https://gdpr-info.eu/art-7-gdpr/). Selbst bei der Nutzung eines üblichen "OK"-Banners muss sichergestellt werden, dass dieser mit der Aussteuerung der Tags funktional verknüpft ist und keine reine "Placebo" Einwilligung darstellt.

4. Nach unserer Einschätzung ist die **Datenschutzerklärung** auf der Seite noch **nicht ausreichend**, um die neuen Informationspflichten der DSGVO zu erfüllen. Zum einen werden noch nicht alle Technologien (sog. "Processors"), die Daten erheben, genannt, zum anderen nicht alle Daten aufgelistet, die externe Partner / Technologien / Provider erhalten. Ebenfalls schreibt die DSGVO vor, den Zweck jeder einzelnen Datenerverarbeitung genau anzugeben. Diese Informationen konnten wir auf der Seite nicht vollumfänglich finden.

5. Anbieter von Webseiten (sog. "Controller"), aber auch u.U. deren Agenturen als Co-Controller, **haften** auch für Tags, die von anderen Tags nachgeladen werden (sog. **"Piggybacking"**).

## Empfehlungen & Action Points

* Usercentrics ermöglicht es, eingebundene Tags dauerhaft zu überwachen und mit der Datenschutzbedingung abzugleichen. Kommen neue Tags hinzu, kann umgehend gewarnt werden. Die Datenschutzerklärung wird vollautomatisch mit den korrekten Inhalten und Opt-Out Links je Technologie erweitert.

* Usercentrics bietet Kunden einen Opt-In Banner (sog. "Privacy Button") mit der Möglichkeit, die Einstellungen des Nutzers anschließend z.B. in einem Tag Management System zu berücksichtigen.

* Die Hinweise auf alle (neuen) Rechte des Nutzer u.a. auf Vergessen, Portabilität, Einsicht, Änderung der Daten sind noch nicht vollständig implementiert; zudem ist es wichtig, dass die Funktionalitäten für die Umsetzung der Rechte intern geschaffen und dauerhaft vorgehalten werden.

* Der Opt-Out für jede geladene Technologie inkl. möglicher nachgeladener Technologien muss gewährleistet sein. Ein Click-Out auf Drittseiten reicht nicht aus.

* Jeder einzelne Datenpunkt der erhoben wird, muss dem Nutzer erklärt werden, u.a. was er bedeutet, wofür er genutzt wird und wie lange er gespeichert wird. Eine sehr allgemeine Formulierung z.B. "Criteo, um Werbung auszuspielen ..." ist nicht ausreichend.

* Hinweise zur Aufsichtsbehörde und dem Recht des Nutzers sich zu beschweren noch unvollständig.

## Wie wurde der Report aufgebaut?

Der Bericht soll den Betreiber von Webseiten dabei unterstützen sicherer, schneller und v.a. datenschutzkonformer zu werden.
Dafür haben wir folgenden 3-Schritte-Prozess entwickelt:

1. **Daten erheben**: Wir prüfen bis zu 50 Unterseiten und zudem im Detail die Startseite, da sie häufig die meisten Besucherzahlen aufweist.

2. **Daten auswerten**: Wir analysieren die Daten durch verschiedene eigene Algorithmen, um den Status Quo der externen Tag-Landschaft zu ermitteln.

3. **Empfehlungen ableiten**: Wir erstellen konkrete Ergebnisse und Handlungsempfehlungen und stellen diese auf einer interaktiven Webseite zur Verfügung.

## Analyse Nr.1: Anzahl der Aufrufe pro Technologie

Es wurde die Anzahl der Aufrufe der jeweiligen Technologien analysiert. Untere Grafik zeigt, welche Knoten Technologien sehr häufig genutzt werden.

* Größe der Kreise: Anzahl der Aufrufe / wie oft wurde das jeweilige Tag aufgerufen

* Dicke des Pfeils: Anzahl, wie oft der Knoten benutzt wurde

* Falls keine Nummer angegeben wurde, handelt es sich um einen Zwischen-Knoten z.B. eine Subdomain

{%prevalence ./assets/converted_csv.json %}

## Analyse Nr. 2: Piggybacking

Technologien, die weitere Technologien nachladen.

* Größe der Knoten: Anzahl der Aufrufe

* Knoten Farbe violett: zeigt nachgeladene Tags

* roter Pfeil: Zeigt Nachladen von Technologien

{%prevalence ./assets/converted_dump_prevalence.json %}

## Analyse Nr. 3: Ladezeiten

Analyse der Ladezeiten von Technologien auf der Startseite. Die Geschwindigkeit, in der Inhalte auf einer Webseite geladen werden ist wichtig, da er direkt verbunden ist mit der Absprungrate der Nutzer. Dabei sind Geschwindigkeiten unterschiedlich zu bewerten z.B.

* außerordentlich relevant: bei Inhalten oder Marketing-/Bidding-relevanten Skripten.

* weniger relevant: Skripte z.B. die für die Anzeige eines Opt-In Banners.

Außerdem gilt:

* Größe der Knoten: Dauer der Ladezeit

* Farbe rot: ungewöhnlich hohe Ladezeiten

{%latency ./assets/converted_dump_latency.json %}

## Legende Ladezeiten

Beschreibung der Verwendeten Spalten mit Beispiel

| Name       | Beschreibung           | Bsp  |
| -----------|:-------------        | :-----:|
| domain     | (sub-)domain, von der Daten geladen werden         | acdn.adnxs.com |
| latency    | Ladedauer in Millisekunden      |    240  |
| url        | URL, mit der kommuniziert wurde      |  	https://acdn.adnxs.com/ib/static/usersync/v4/async_usersync.html  |

## Tabelle Ladezeiten

{%timelineTable ./assets/converted_dump_timeline.json %}

## Analyse No.4: Datenweitergabe

Zusammenfassung der Daten, die von Technologieanbietern "gesammelt" werden und die unter die Regelungen der DSGVO fallen. Nachfolgend analysieren wir die jeweiligen URL-Parameter & Cookies inklusive deren Inhalte. Laut DSGVO muss sichergestellt sein, dass die Informationen dem Nuzter derart zugänglich gemacht werden, dass der Nutzer zu jedem Zeitpunkt weiß, wer welche Daten für welchen Zweck erhalten hat.

**Bitte beachten**: Auch bei Cookie Identifiern wie z.B. ID's, handelt es sich um personenbezogene Daten; nicht nur bei der IP oder Name etc.

## Legende Datenweitergabe

Beschreibung der Spalten mit Beispiel:

| Name        | Beschreibung           | Bsp  |
| ------------- |:-------------| :-----:|
| param value     | Datenwert            | Max Mustermann |
| param name      | Name Datenpunkt    |    Name  |
| sudomain using it | Welche subdomain die Daten erhalten hat      |  halc.iadvize.com   |
| top level domain      | Name der Domain, meist ähnlich zur Firma    |    advertising.com  |
| number of calls      | Wie oft dieser **Wert** verschickt wurde    |    3  |
| type      | Art des Datenversands    |    Cookie  |

## Tabelle Datenweitergabe

{%parameters ./assets/converted_dump_parameters.json %}


# Kontakt & Copyright

https://usercentrics.de<br>
Usercentrics GmbH<br>
Sonnenstr. 23<br>
80331 München <br>
Germany<br>

**Kontakt bei Fragen zum Report:** <br>
mr@usercentrics.com<br>
+49 171 5 24 69 69

# Disclaimer
Es handelt sich um keine Rechtsberatung, sondern um eine reine Stoff- und Inhaltssammlung mit keinerlei Anspruch auf Vollständigkeit oder Richtigkeit.